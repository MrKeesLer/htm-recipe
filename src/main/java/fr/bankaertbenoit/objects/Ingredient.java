package fr.bankaertbenoit.objects;

/**
 * @author Bankaert Benoit
 * @version 0.1.0
 * This class represente an Ingredient object
 */
public class Ingredient {

    private String id;
    private String name;

    /**
     * Instanciate an Ingredient with an id and an name
     * @param i is the id of the ingredient ( has to be unique )
     * @param n is the name of the ingredient
     */
    public Ingredient(String i,String n){
        this.id = i;
        this.name = n;
    }

    /**
     * @return the id of the ingredient
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name of the ingredient
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
