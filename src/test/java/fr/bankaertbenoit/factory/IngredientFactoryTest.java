package fr.bankaertbenoit.factory;

import fr.bankaertbenoit.objects.Ingredient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IngredientFactoryTest {

    @Test
    public void createIngredientsViaFactoryTest(){
        String id = "poivre";
        String name = "poivre";
        Ingredient i = IngredientFactory.createRecipe(id,name);
        assertEquals(id,i.getId());
        assertEquals(name,i.getName());
    }

}
