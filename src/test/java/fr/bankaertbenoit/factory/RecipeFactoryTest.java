package fr.bankaertbenoit.factory;

import fr.bankaertbenoit.objects.Ingredient;
import fr.bankaertbenoit.objects.Recipe;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecipeFactoryTest {

    @Test
    public void createRecipeViaFactoryTest(){
        int id = 1;
        List<String> steps = Arrays.asList("A","B","C");
        int time = 50;
        Map<Ingredient,Integer> ingredients = new HashMap<>();
        String name = "Nom";
        ingredients.put(new Ingredient("tomate","tomate"),5);
        Recipe r = RecipeFactory.createRecipe(id,steps,time,ingredients,name);
        assertEquals(id,r.getId());
        assertEquals(steps,r.getStep());
        assertEquals(time,r.getTime());
        assertEquals(ingredients,r.getIngredients());
        assertEquals(name,r.getName());
    }
}
