package fr.bankaertbenoit.objects;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecipeTest {

    @Test
    public void createRecipe(){
        int id = 1;
        List<String> steps = Arrays.asList("A","B","C");
        int time = 50;
        Map<Ingredient,Integer> ingredients = new HashMap<>();
        String name = "Nom";
        ingredients.put(new Ingredient("tomate","tomate"),5);
        Recipe r = new Recipe(id,steps,time,ingredients,name);
        assertEquals(id,r.getId());
        assertEquals(steps,r.getStep());
        assertEquals(time,r.getTime());
        assertEquals(ingredients,r.getIngredients());
        assertEquals(name,r.getName());
    }

    @Test
    public void getStepWithFormatTest(){
        String steps = "Etape 1\nCouper le beurre en deux\nEtape 2\nManger le beurre\n";
        List<String> step = Arrays.asList("Couper le beurre en deux","Manger le beurre");
        Recipe r = new Recipe(1,step,10,null,"Beurre");
        assertEquals(steps,r.getStepWithFormats());
    }

}
